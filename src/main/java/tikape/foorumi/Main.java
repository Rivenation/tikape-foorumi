package tikape.foorumi;


import spark.ModelAndView;
import spark.Spark;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import tikape.foorumi.database.BoardDao;
import tikape.foorumi.database.CommentDao;
import tikape.foorumi.database.Database;
import tikape.foorumi.database.PostDao;
import tikape.foorumi.domain.Board;
import tikape.foorumi.domain.Comment;
import tikape.foorumi.domain.Post;

import java.sql.Timestamp;
import java.util.HashMap;

import static spark.Spark.get;
import static spark.Spark.post;


public class Main {

    public static void main(String[] args) throws Exception {
        // Expose the public folder so we can have CSS files
        Spark.staticFileLocation("/public");
        
        // asetetaan portti jos heroku antaa PORT-ympäristömuuttujan
        if (System.getenv("PORT") != null) {
            Spark.port(Integer.valueOf(System.getenv("PORT")));
        }
        
        Database database = new Database("jdbc:sqlite:database.db");
        database.recreate();
        database.addMockData();
        BoardDao boardDao = new BoardDao(database);
        PostDao postDao = new PostDao(database);
        CommentDao commentDao = new CommentDao(database);

        get("/", (req, res) -> {
            HashMap map = new HashMap<>();
            map.put("boards", boardDao.findAll());

            return new ModelAndView(map, "index");
        }, new ThymeleafTemplateEngine());

        get("/board/:boardId/post/:postId", (req, res) -> {
            HashMap map = new HashMap<>();
            String offsetString = req.queryParams("offset");
            int offset = offsetString != null ? Integer.parseInt(offsetString) : 0;

            map.put("post", postDao.findOne(Long.parseLong(req.params("postId"))));
            map.put("board", boardDao.findOne(Long.parseLong(req.params("boardId"))));
            map.put("comments", commentDao.findByPost(Long.parseLong(req.params("postId")), offset));
            map.put("offset", offset);
            return new ModelAndView(map, "post");
        }, new ThymeleafTemplateEngine());

        get("/board/:id", (req, res) -> {
            HashMap map = new HashMap<>();
            long boardId = Long.parseLong(req.params("id"));
            String offsetString = req.queryParams("offset");
            int offset = offsetString != null ? Integer.parseInt(offsetString) : 0;

            map.put("board", boardDao.findOne(boardId));
            map.put("posts", postDao.findByBoard(boardId, offset));
            map.put("offset", offset);
            return new ModelAndView(map, "board");
        }, new ThymeleafTemplateEngine());

        post("/board", (req, res) -> {
            String name = req.queryParams("name");

            if (name.isEmpty()) {
                res.status(400);
                return "Error: Name cannot be empty";
            }

            if (name.length() > 50) {
                res.status(400);
                return "Error: Too long";
            }

            Board newBoard = new Board(req.queryParams("name"));
            boardDao.add(newBoard);
            res.redirect("/");
            return "OK";
        });

        post("/board/:boardId/post", (req, res) -> {
            String title = req.queryParams("title");
            String author = req.queryParams("author");
            String content = req.queryParams("content");

            if (title.isEmpty() || content.isEmpty()) {
                res.status(400);
                return "Error: Title or content cannot be empty";
            }

            if (content.length() > 5000 || author.length() > 50 || title.length() > 100) {
                res.status(400);
                return "Error: Too long";
            }

            if (author.isEmpty()) {
                author = "Anonyymi";
            }

            Post newPost = new Post(Long.parseLong(req.params("boardId")),
                    title, author, content, new Timestamp(System.currentTimeMillis()));

            postDao.add(newPost);
            res.redirect("/board/" + req.params("boardId"));
            return "OK";
        });

        post("/board/:boardId/post/:postId/comment", (req, res) -> {
            String author = req.queryParams("author");
            String content = req.queryParams("content");

            if (content.isEmpty()) {
                res.status(400);
                return "Error: Content cannot be empty";
            }

            if (content.length() > 5000 || author.length() > 50) {
                res.status(400);
                return "Error: Too long";
            }

            if (author.isEmpty()) {
                author = "Anonyymi";
            }

            Comment newComment = new Comment(Long.parseLong(req.params("postId")),
                    author, content,
                    new Timestamp(System.currentTimeMillis()));
            commentDao.add(newComment);
            res.redirect("/board/" + req.params("boardId") + "/post/" + req.params("postId"));
            return "OK";
        });

    }
}
