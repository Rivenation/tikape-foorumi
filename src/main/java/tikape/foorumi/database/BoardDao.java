package tikape.foorumi.database;

import tikape.foorumi.domain.Board;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BoardDao implements Dao<Board, Long> {
    private Database database;

    public BoardDao(Database database) {
        this.database = database;
    }

    @Override
    public Board findOne(Long key) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();
        try {
            stmt = connection.prepareStatement("SELECT * FROM Board WHERE id = ?");
            stmt.setObject(1, key);

            rs = stmt.executeQuery();
            boolean hasOne = rs.next();
            if (!hasOne) {
                return null;
            }

            return parseBoard(rs);
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }
    }

    @Override
    public List<Board> findAll() throws SQLException {
        List<Board> boards = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            String sql = "SELECT Board.*, "
                    + "(SELECT COUNT(Comment.id) FROM Post LEFT JOIN Comment ON Comment.post_id = Post.id "
                    + "WHERE Post.board_id = Board.id) AS comment_count, "
                    + "CASE "
                    + "WHEN MAX(Comment.time_created) IS NULL THEN "
                    + "Post.time_created "
                    + "ELSE "
                    + "MAX(Comment.time_created) "
                    + "END AS latest_comment "
                    + "FROM Board "
                    + "LEFT JOIN Post ON Board.id = Post.board_id "
                    + "LEFT JOIN Comment ON Post.id = Comment.post_id "
                    + "GROUP BY Board.id ORDER BY LOWER(name) ASC";
            stmt = connection.prepareStatement(sql);

            rs = stmt.executeQuery();
            while (rs.next()) {
                Board b = parseBoard(rs);
                b.setCommentAmount(rs.getInt("comment_count"));
                b.setLatestCommentTime(rs.getTimestamp("latest_comment"));
                boards.add(b);
            }
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }

        return boards;
    }
    
    public List<Board> findAllForMockData() throws SQLException {
        List<Board> boards = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            String sql = "SELECT Board.* FROM Board";
            stmt = connection.prepareStatement(sql);

            rs = stmt.executeQuery();
            while (rs.next()) {
                Board b = parseBoard(rs);
                boards.add(b);
            }
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }

        return boards;
    }

    @Override
    public void delete(Long key) throws SQLException {
        PreparedStatement stmt = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("DELETE FROM Board WHERE id = ?");
            stmt.setLong(1, key);
            stmt.executeUpdate();
        } finally {
            if (stmt != null) stmt.close();
            connection.close();
        }
    }

    @Override
    public void add(Board model) throws SQLException {
        PreparedStatement stmt = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("INSERT INTO Board (id,name) VALUES (NULL,?)");
            stmt.setString(1, model.getName());
            stmt.executeUpdate();
        } finally {
            if (stmt != null) stmt.close();
            connection.close();
        }
    }

    private Board parseBoard(ResultSet rs) throws SQLException {
        Long id = rs.getLong("id");
        String name = rs.getString("name");

        return new Board(id, name);
    }
}
