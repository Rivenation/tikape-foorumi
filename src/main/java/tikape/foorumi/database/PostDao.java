package tikape.foorumi.database;

import tikape.foorumi.domain.Post;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostDao implements Dao<Post, Long> {
    private Database database;

    public PostDao(Database database) {
        this.database = database;
    }

    @Override
    public void add(Post model) throws SQLException {
        PreparedStatement stmt = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("INSERT INTO Post (id,board_id,title,author,content,time_created) VALUES (NULL,?,?,?,?,?)");
            stmt.setLong(1, model.getBoardId());
            stmt.setString(2, model.getTitle());
            stmt.setString(3, model.getAuthor());
            stmt.setString(4, model.getContent());
            stmt.setLong(5, model.getTimeCreated().getTime());
            stmt.executeUpdate();
        } finally {
            if (stmt != null) stmt.close();
            connection.close();
        }
    }

    @Override
    public Post findOne(Long key) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("SELECT Post.* FROM Post WHERE id = ?");
            stmt.setLong(1, key);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return null;
            }

            return parsePost(rs);
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }
    }

    @Override
    public List<Post> findAll() throws SQLException {
        List<Post> posts = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("SELECT Post.* FROM Post");
            rs = stmt.executeQuery();
            while (rs.next()) {
                Post p = parsePost(rs);
                posts.add(p);
            }
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }

        return posts;
    }

    @Override
    public void delete(Long key) throws SQLException {
        Connection connection = database.getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement("DELETE FROM Post WHERE id = ?");
            stmt.setLong(1, key);
            stmt.execute();
        } finally {
            if (stmt != null) stmt.close();
            connection.close();
        }
    }

    public List<Post> findByBoard(Long boardId, int offset) throws SQLException {
        List<Post> posts = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("SELECT Post.*,"
                    + "COUNT(Comment.id) AS comment_count, "
                    + "CASE "
                    + "WHEN MAX(Comment.time_created) IS NULL THEN "
                    + "Post.time_created "
                    + "ELSE "
                    + "MAX(Comment.time_created) "
                    + "END AS latest_comment "
                    + "FROM Post "
                    + "LEFT JOIN Comment "
                    + "ON Post.id = Comment.post_id "
                    + "WHERE Post.board_id = ? "
                    + "GROUP BY Post.id ORDER BY latest_comment DESC LIMIT 10 OFFSET ?");
            stmt.setLong(1, boardId);
            stmt.setInt(2, offset * 10);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Post p = parsePost(rs);
                p.setCommentAmount(rs.getInt("comment_count"));
                p.setLastCommentTime(rs.getTimestamp("latest_comment"));
                posts.add(p);
            }
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }

        return posts;
    }

    public Timestamp getLatestComment(long postId) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();
        try {
            stmt = connection.prepareStatement("SELECT Comment.time_created FROM Post "
                    + " INNER JOIN Comment ON Post.id = Comment.post_id "
                    + " WHERE Post.id = ?"
                    + " ORDER BY Comment.time_created DESC LIMIT 1");
            stmt.setLong(1, postId);

            rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getTimestamp("time_created");
            }
            return null;
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }
    }

    private Post parsePost(ResultSet rs) throws SQLException {
        Long id = rs.getLong("id");
        Long boardId = rs.getLong("board_id");
        String title = rs.getString("title");
        String author = rs.getString("author");
        String content = rs.getString("content");
        Timestamp timeCreated = rs.getTimestamp("time_created");
        return new Post(id, boardId, title, author, content, timeCreated);
    }
}
