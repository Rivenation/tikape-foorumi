package tikape.foorumi.database;

import tikape.foorumi.domain.Comment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommentDao implements Dao<Comment, Long> {
    private Database database;

    public CommentDao(Database database) {
        this.database = database;
    }

    @Override
    public Comment findOne(Long key) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("SELECT * FROM Comment WHERE id = ?");
            stmt.setLong(1, key);

            rs = stmt.executeQuery();
            if (rs.next()) {
                return parseComment(rs);
            } else {
                return null;
            }
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }
    }

    @Override
    public List<Comment> findAll() throws SQLException {
        List<Comment> comments = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("SELECT * FROM Comment");

            rs = stmt.executeQuery();
            while (rs.next()) {
                comments.add(parseComment(rs));
            }
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }

        return comments;
    }

    public List<Comment> findByPost(Long postId, int offset) throws SQLException {
        List<Comment> comments = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection connection = database.getConnection();

        try {
            stmt = connection.prepareStatement("SELECT * FROM Comment WHERE post_id = ? "
                    + "ORDER BY time_created DESC LIMIT 20 OFFSET ?");
            stmt.setLong(1, postId);
            stmt.setInt(2, offset * 20);

            rs = stmt.executeQuery();
            while (rs.next()) {
                comments.add(parseComment(rs));
            }
        } finally {
            if (stmt != null) stmt.close();
            if (rs != null) rs.close();
            connection.close();
        }

        return comments;
    }

    @Override
    public void delete(Long key) throws SQLException {
        Connection connection = database.getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement("DELETE FROM Comment WHERE id = ?");
            stmt.setLong(1, key);

            stmt.execute();
        } finally {
            if (stmt != null) stmt.close();
            connection.close();
        }
    }

    @Override
    public void add(Comment model) throws SQLException {
        Connection connection = database.getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement("INSERT INTO Comment VALUES (NULL, ?,?,?,?)");
            stmt.setLong(1, model.getPostId());
            stmt.setString(2, model.getAuthor());
            stmt.setString(3, model.getContent());
            stmt.setLong(4, model.getTimeCreated().getTime());
            stmt.execute();
        } finally {
            if (stmt != null) stmt.close();
            connection.close();
        }
    }

    private Comment parseComment(ResultSet rs) throws SQLException {
        Long id = rs.getLong("id");
        Long postId = rs.getLong("post_id");
        String author = rs.getString("author");
        String content = rs.getString("content");
        Timestamp dateCreated = rs.getTimestamp("time_created");
        return new Comment(id, postId, author, content, dateCreated);
    }
}
