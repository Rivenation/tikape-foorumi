package tikape.foorumi.database;

import tikape.foorumi.domain.Board;
import tikape.foorumi.domain.Comment;
import tikape.foorumi.domain.Post;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Database {

    private String databaseAddress;
    private String[] mockUsernames = new String[]{ "Väinämöinen", "Ilmarinen", "Lemminkäinen", "Ukko",
        "Joukahainen", "Louhi", "Kullervo", "Marjatta", "Kyllikki", "Pohjolan tytär" };
    private String[] mockBoards = new String[]{ "Historia", "Talous", "Uutiset", "Harrastukset",
        "Ruoka", "Viihde", "Julkkikset", "Tietokoneet", "Ohjelmointi", "Politiikka" };
    private String[] mockContent = new String[]{ 
        "Mieleni minun tekevi, aivoni ajattelevi ", 
        "lähteäni laulamahan, saa'ani sanelemahan ", 
        "Veli kulta, veikkoseni, kaunis kasvinkumppalini ", 
        "Lähe nyt kanssa laulamahan, saa kera sanelemahan ",
        "yhtehen yhyttyämme, kahta'alta käytyämme ", 
        "Pohjan peltojen periltä, Kalevalan kankahilta ", 
        "Vilu mulle virttä virkkoi, sae saatteli runoja ", 
        "Virttä toista tuulet toivat, meren aaltoset ajoivat ", 
        "Linnut liitteli sanoja, puien latvat lausehia ", 
        "Vieri impi veen emona. Uipi iät, uipi lännet " 
    };
    
    private BoardDao bd = new BoardDao(this);
    private CommentDao cd = new CommentDao(this);
    private PostDao pd = new PostDao(this);

    public Database(String databaseAddress) throws ClassNotFoundException {
        this.databaseAddress = databaseAddress;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(databaseAddress);
    }

    public void init() {
        List<String> createTablesSQL = createTablesSQL();

        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();

            for (String sql : createTablesSQL) {
                System.out.println("Creating table >> " + sql);
                st.executeUpdate(sql);
            }
        } catch (Throwable t) {
            System.out.println("Error >> " + t.getMessage());
        }
    }

    public void recreate() throws SQLException {
        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();
            List<String> dropTablesSQL = dropTablesSQL();
            for (String sql : dropTablesSQL) {
                System.out.println("Dropping table >> " + sql);
                st.executeUpdate(sql);
            }

        }
        init();
    }

    public void addMockData() throws SQLException {
        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();
            List<String> mockDataSQL = mockDataSQL();
            for (String sql : mockDataSQL) {
                System.out.println("Adding mock data >> " + sql);
                st.executeUpdate(sql);
            }

        }
    }

    private List<String> createTablesSQL() {
        List<String> sql = new ArrayList<>();
        sql.add("CREATE TABLE Board(id INTEGER PRIMARY KEY NOT NULL, name TEXT NOT NULL UNIQUE)");
        sql.add("CREATE TABLE Post(id INTEGER PRIMARY KEY NOT NULL, board_id INTEGER NOT NULL, title TEXT NOT NULL, author TEXT NOT NULL, content TEXT NOT NULL, time_created INTEGER NOT NULL)");
        sql.add("CREATE TABLE Comment(id INTEGER PRIMARY KEY NOT NULL, post_id INTEGER NOT NULL, author TEXT NOT NULL, content TEXT NOT NULL, time_created INTEGER NOT NULL)");
        return sql;
    }

    private List<String> dropTablesSQL() {
        List<String> sql = new ArrayList<>();
        sql.add("DROP TABLE IF EXISTS Board");
        sql.add("DROP TABLE IF EXISTS Post");
        sql.add("DROP TABLE IF EXISTS Comment");
        return sql;
    }

    private List<String> mockDataSQL() throws SQLException {
        List<String> sql = new ArrayList<>();
        long timeStart = System.currentTimeMillis();
        System.out.println("Creating mock data, this might take a while...");
        Random rand = new Random();
        for (int i = 0; i < mockBoards.length; i++) {
            bd.add(generateMockBoard(i));
            List<Board> boards = bd.findAllForMockData();
            Board b = boards.get(boards.size() - 1);
            int postVariety = rand.nextInt(10);
            for (int j = 0; j < 15 - postVariety; j++) {
                pd.add(generateRandomPost(j, b.getId()));
                List<Post> posts = pd.findAll();
                Post p = posts.get(posts.size() - 1);
                int commentVariety = rand.nextInt(30);
                for (int k = 0; k < 50 - commentVariety; k++) {
                    Comment c = generateRandomComment(k, p.getId());
                    cd.add(c);
                }
            }
        }
        System.out.println("Took " + (((float)(System.currentTimeMillis() - timeStart)) / 1000.0) + " seconds to create mock data") ;
        return sql;
    }

    private Board generateMockBoard(int index) throws SQLException {
        
        Board b = new Board(mockBoards[index]);
        return b;
    }

    private Post generateRandomPost(long id, long boardId) {
        Random rand = new Random();
        
        return new Post(id, boardId, mockContent[rand.nextInt(mockContent.length)], mockUsernames[rand.nextInt(mockUsernames.length)],
                mockContent[rand.nextInt(mockContent.length)], new Timestamp(System.currentTimeMillis() - rand.nextInt(100000)));
    }

    private Comment generateRandomComment(long id, long postId) {
        Random rand = new Random();
        return new Comment(id, postId, mockUsernames[rand.nextInt(mockUsernames.length)], mockContent[rand.nextInt(mockContent.length)],
                new Timestamp(System.currentTimeMillis() - rand.nextInt(100000000)));
    }
}
