package tikape.foorumi.domain;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class Post {
    private Long id;
    private Long boardId;
    private String title;
    private String author;
    private String content;
    private Timestamp timeCreated;
    private int commentAmount;
    private Timestamp lastCommentTime;

    public Post(Long id, Long boardId, String title, String author, String content, Timestamp timeCreated) {
        this.id = id;
        this.boardId = boardId;
        this.title = title;
        this.author = author;
        this.content = content;
        this.timeCreated = timeCreated;
    }

    public Post(Long boardId, String title, String author, String content, Timestamp timeCreated) {
        this.boardId = boardId;
        this.title = title;
        this.author = author;
        this.content = content;
        this.timeCreated = timeCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Timestamp timeCreated) {
        this.timeCreated = timeCreated;
    }

    public int getCommentAmount() {
        return commentAmount;
    }

    public void setCommentAmount(int commentAmount) {
        this.commentAmount = commentAmount;
    }

    public Timestamp getLastCommentTime() {
        return lastCommentTime;
    }

    public void setLastCommentTime(Timestamp lastCommentTime) {
        this.lastCommentTime = lastCommentTime;
    }

    public String getFormattedDateCreated() {
        return timeCreated.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd.MM.uuuu HH:mm:ss"));
    }

    public String getFormattedLatestCommentDate() {
        if (lastCommentTime != null) {
            return lastCommentTime.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd.MM.uuuu HH:mm:ss"));
        } else {
            return "-";
        }
    }

    @Override
    public String toString() {
        return "[Post id=" + id + " parentId=" + boardId + "]";
    }
}
