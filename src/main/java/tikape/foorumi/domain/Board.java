package tikape.foorumi.domain;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class Board {
    private long id;
    private String name;
    private int commentAmount;
    private Timestamp latestCommentTime;

    public Board(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Board(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getLatestCommentTime() {
        return latestCommentTime;
    }

    public void setLatestCommentTime(Timestamp latestCommentTime) {
        this.latestCommentTime = latestCommentTime;
    }

    public String getFormattedLastCommentDate() {
        if (latestCommentTime != null) {
            return latestCommentTime.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd-MM-uuuu HH:mm:ss"));
        } else {
            return "-";
        }
    }

    public int getCommentAmount() {
        return commentAmount;
    }

    public void setCommentAmount(int commentAmount) {
        this.commentAmount = commentAmount;
    }

    @Override
    public String toString() {
        return "[Board id=" + id + "]";
    }
}
