package tikape.foorumi.domain;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class Comment {
    private Long id;
    private Long postId;
    private String author;
    private String content;
    private Timestamp timeCreated;

    public Comment(Long id, Long postId, String author, String content, Timestamp timeCreated) {
        this.id = id;
        this.postId = postId;
        this.author = author;
        this.content = content;
        this.timeCreated = timeCreated;
    }

    public Comment(Long postId, String author, String content, Timestamp timeCreated) {
        this.postId = postId;
        this.author = author;
        this.content = content;
        this.timeCreated = timeCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Timestamp timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getFormattedDateCreated() {
        return timeCreated.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd.MM.uuuu HH:mm:ss"));
    }

    @Override
    public String toString() {
        return "[Comment id=" + id + " parentId=" + postId + "]";
    }
}
