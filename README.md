# HutuKiren Tikape-Foorumi

Tietokantojen perusteet -kurssilla tehtävän web-sovelluksen tuotos.

Versio 1.0.0 (29.10.2016)

Sovellus löytyy osoitteesta [http://tikape.hutukiren.com](http://tikape.hutukiren.com)

Viimeinen Raportti  https://docs.google.com/document/d/1myPVeqXSMbn5M_x8ouKOaJhtm0G19sK8_npgg97Zmp4/edit?usp=sharing